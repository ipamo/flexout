ARG  TAG=3.7.3-slim-buster
FROM python:${TAG}

ENV TZ="Europe/Paris"
WORKDIR /usr/src/app

COPY requirements.txt requirements_default.txt requirements_tabulate.txt requirements_excel.txt requirements_smb.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "-m", "unittest" ]
