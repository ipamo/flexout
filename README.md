Flexout
=======

A Python library to write text or tabular data to a flexible, easily configurable output: CSV or Excel file, or tabulated stdout/stderr.

The output file may be on the local file system or on a Windows/Samba share (including when the library is used on Linux).

**Moved to [zut](https://gitlab.com/ipamo/zut)**
