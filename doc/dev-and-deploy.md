Deploy
======

## Prepare dev environment

    python -m venv .venv     # Debian: python3 -m venv .venv
    .venv\Scripts\activate   # Linux:  source .venv/bin/activate
    pip install -r requirements.txt


## Run unit tests

Create configuration file `.env` to avoid skipping smb-related tests, example:

    SMB_TEST_RESULTS=\\myserver\share\test-results
    # If not on a workstation that is member of the domain:
    #SMB_USERNAME= 
    #SMB_PASSWORD= 

On the local system:

    python -m unittest

For specific Python versions (using Docker):

    docker build --tag flexout:3.7 --build-arg TAG=3.7-slim-buster .
    docker run -it --rm flexout:3.7

## Clean before build

Powershell:

    Get-ChildItem -Include __pycache__,build,dist,*.egg-info -Recurse -force | Where-Object fullname -notlike "*\.venv\*" | Remove-Item -Force -Recurse

Bash:

    find . \( -name __pycache__ -o -name build -o -name dist -o -name "*.egg-info" \) -not -path "./.venv/*" -exec rm -rf {} \;


## Build wheel

    pip wheel --no-deps -w dist .
    twine check dist/...


## Upload wheel on PyPI

    $env:HTTPS_PROXY="http://..."
    $env:REQUESTS_CA_BUNDLE="C:/..."
    twine upload --repository ... dist/...
