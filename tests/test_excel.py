from __future__ import annotations
import logging
from unittest import TestCase
from datetime import datetime, date, time, timezone

from openpyxl import load_workbook, Workbook
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.worksheet.table import Table, TableStyleInfo
from openpyxl.cell.cell import Cell
from openpyxl.utils import range_boundaries

from flexout import flexout, filemgr, ExcelOut
from tests.settings import LOCAL_TEST_RESULTS, SMB_TEST_RESULTS, SAMPLES


FRUITS_HEADERS = ["Fruit", "Date", "Mix", "Decimal", "2011", "2012", "2013", "2014"]

FRUITS_ROWS = [
    ['Apples',  date(2000, 1, 1), datetime(2014, 10, 26, 10, 26, 59), 3.14,    10000, 5000, 8000, 6000],
    ['Pears',   date(2000, 1, 1), date(2014, 10, 26),                 6.55957, 2000, 3000, 4000, 5000],
    ['Bananas', date(2000, 1, 1), time(10, 26, 59),                   2.7182,  36000, 6000, 6500, 6000],
    ['Oranges', date(2000, 1, 1), None,                               123456789.1234567, 500,  300,  200,  700],
]


class Case(TestCase):
    def setUp(self):
        if not ExcelOut.is_available():
            return self.skipTest('ExcelOut not available')


    def test_successive_local(self):
        path = LOCAL_TEST_RESULTS.joinpath('successive.xlsx')
        self._successive(path)


    def test_successive_smb(self):
        if not SMB_TEST_RESULTS:
            return self.skipTest('smb not available')
        
        self._successive(SMB_TEST_RESULTS + r'\successive.xlsx')


    def test_several_local(self):
        path = LOCAL_TEST_RESULTS.joinpath('several.xlsx')
        self._several(path)


    def test_several_smb(self):
        if not SMB_TEST_RESULTS:
            return self.skipTest('smb not available')

        self._several(SMB_TEST_RESULTS + r'\several.xlsx')


    def test_timezone(self):
        path = LOCAL_TEST_RESULTS.joinpath('timezone.xlsx')
        
        if filemgr.exists(path):
            filemgr.remove(path)
            
        
        with flexout(f"{path}#Localtime") as o:
            o.headers = ['Col']
            o.append(datetime.fromisoformat('1998-07-12T21:46:00.123+02:00'))

        self._verify(f"{path}#Localtime", expected_headers=['Col'], expected_rows=[[datetime(1998, 7, 12, 21, 46, 0, 123000)]])

        
        with flexout(f"{path}#UTC", timezone=timezone.utc) as o:
            o.headers = ['Col']
            o.append(datetime.fromisoformat('1998-07-12T21:46:00.123+02:00'))

        self._verify(f"{path}#UTC", expected_headers=['Col'], expected_rows=[[datetime(1998, 7, 12, 19, 46, 0, 123000)]])


    def test_manual_lessrows(self):
        origin_path = SAMPLES.joinpath('manual-lessrows.xlsx')
        path = LOCAL_TEST_RESULTS.joinpath('manual-lessrows.xlsx')

        if filemgr.exists(path):
            filemgr.remove(path)

        filemgr.copy2(origin_path, path)

        with flexout(f'{path}#Tableau1', headers=['Col1', 'Col2']) as o:
            o.append(['A', 1])
            o.append(['B', 2])

        #MANUAL: check that table as filtering buttons, and that styles and formulas are automatically added when a new row is created

    
    def _several(self, workbook_path: str):
        if filemgr.exists(workbook_path):
            filemgr.remove(workbook_path)
        
        with flexout(workbook_path, headers=['Col1', 'Col2']) as o:
            o.append(1, 2)
            o.append(3, 4)
        
        with flexout(f"{workbook_path}#Table2", headers=['T2_Col1', 'T2_Col2']) as o:
            o.append(21, 22)
            o.append(23, 24)
        
        with flexout(f"{workbook_path}#Table3", headers=['T3_Col1', 'T3_Col2']) as o:
            o.append(21, 22)
            o.append(23, 24)

        self._verify(workbook_path, expected_headers=['Col1', 'Col2'], expected_rows=[[1, 2], [3, 4]])
        self._verify(f"{workbook_path}#Table2", expected_headers=['T2_Col1', 'T2_Col2'], expected_rows=[[21, 22], [23, 24]])
        self._verify(f"{workbook_path}#Table3", expected_headers=['T3_Col1', 'T3_Col2'], expected_rows=[[21, 22], [23, 24]])


    def _successive(self, workbook_path: str):
        # Write to new file    
        if filemgr.exists(workbook_path):
            filemgr.remove(workbook_path)

        with flexout(workbook_path) as o:
            o.headers = FRUITS_HEADERS
            for row in FRUITS_ROWS:
                o.append(row)

        self._verify(workbook_path, FRUITS_HEADERS, FRUITS_ROWS)


        # Append to file
        with flexout(workbook_path, append=True) as o:
            o.headers = ['2011', 'Fruit', 'New']
            o.append([1, 'More1', 'N1'])
            o.append([2, 'More2', 'N2'])
        
        expected_headers = [*FRUITS_HEADERS, 'New']
        expected_rows = []
        for row in FRUITS_ROWS:
            expected_rows.append([*row, '?'])
        expected_rows.append(['More1', None, None, None, 1, None, None, None, 'N1'])
        expected_rows.append(['More2', None, None, None, 2, None, None, None, 'N2'])

        self._verify(workbook_path, expected_headers=expected_headers, expected_rows=expected_rows)


        # Write (NOT APPEND) to existing file with an additional column and row
        with self.assertLogs('flexout.excel', logging.WARNING): # "ignore row values at index >= 9 (first occurence on row 5)" (column value: "MORE")
            with flexout(workbook_path) as o:
                o.headers = [*FRUITS_HEADERS, "2015"]
                o.append(["NEW FRUIT", "Date", "Mix", "Decimal", "2011", "2012", "2013", "2014", "2015", 'MORE'])

        expected_headers = [*FRUITS_HEADERS, 'New', '2015']
        expected_rows = [
            ["NEW FRUIT", "Date", "Mix", "Decimal", "2011", "2012", "2013", "2014", None, "2015"]
        ]
        
        self._verify(workbook_path, expected_headers=expected_headers, expected_rows=expected_rows)
    

    def _verify(self, path: str, expected_headers: list[str], expected_rows: list[list]):
        o = flexout(path)
        workbook_path = o._path
        table_name = o._table_name
        if not table_name:
            table_name = ExcelOut.DEFAULT_TABLE_NAME
        
        actual_headers, actual_rows = _get_excel_table_data(workbook_path, table_name)

        self.assertEqual(actual_headers, expected_headers)
        self.assertEqual(len(actual_rows), len(expected_rows))
        
        for i in range(0, len(actual_rows)):
            self.assertEqual(actual_rows[i], _convert_row_dates(expected_rows[i]), msg=f"row {i+1}")


def _convert_row_dates(row: list) -> list:
    """
    Convert date objects to datetime (Excel does not distinguish between dates and datetimes).
    """
    converted_row = []

    for i in range(0, len(row)):
        value = row[i]
        if type(value) == date:
            value = datetime.combine(value, datetime.min.time())
        converted_row.append(value)

    return converted_row


def _get_excel_table_data(workbook_path: str, table_name: str) -> tuple[list[str], list[list]]:
    """
    Return (headers, rows).
    """
    workbook: Workbook
    with filemgr.open_file(workbook_path, 'rb') as fd:
        workbook = load_workbook(fd)

    # Search table
    worksheet: Worksheet = None
    table: Table = None
    for name in workbook.sheetnames:
        worksheet = workbook[name]
        if table_name in worksheet.tables:
            table = worksheet.tables[table_name]
            break

    if not table:
        raise KeyError(f"table not found: {table_name}")

    # Find headers    
    headers: list[str] = [column.name for column in table.tableColumns]

    # Find data
    rows: list[list] = []

    min_col, min_row, max_col, max_row = range_boundaries(table.ref)
    i_row = min_row + table.headerRowCount
    while i_row <= max_row:
        row = []
        
        i_col = min_col
        while i_col <= max_col:
            cell: Cell = worksheet.cell(i_row, i_col)
            row.append(cell.value)
            i_col += 1

        rows.append(row)
        i_row += 1

    return headers, rows
