from __future__ import annotations
import sys
from contextlib import redirect_stdout
from unittest import TestCase
from io import StringIO
from flexout import flexout


class Case(TestCase):
    def test_noheaders(self):
        f = StringIO()
        with redirect_stdout(f):
            with flexout() as o:
                o.print_title('Title')
                o.file.write('Hello\nWorld')
        
        self.assertEqual(f.getvalue(), '\n########## Title ##########\n\nHello\nWorld')
