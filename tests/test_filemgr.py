from __future__ import annotations
from unittest import TestCase
from pathlib import Path
from flexout import filemgr
from time import sleep
from .settings import LOCAL_TEST_RESULTS, SMB_TEST_RESULTS

class Case(TestCase):
    def test_copy_local(self):
        if not SMB_TEST_RESULTS:
            return self.skipTest('smb not available')
        
        self._do(
            func=filemgr.copy,
            src=LOCAL_TEST_RESULTS.joinpath('test_copy_local-src.txt'),
            create_src=True,
            dst=LOCAL_TEST_RESULTS.joinpath('test_copy_local-dst.txt'),
        )


    def test_copy_smb(self):        
        if not SMB_TEST_RESULTS:
            return self.skipTest('smb not available')

        # To SMB
        self._do(
            func=filemgr.copy,
            src=LOCAL_TEST_RESULTS.joinpath('test_copy_smb-src.txt'),
            create_src=True,
            dst=SMB_TEST_RESULTS + r'\test_copy_smb-dst.txt',
        )

        # From SMB
        self._do(
            func=filemgr.copy,
            src=SMB_TEST_RESULTS + r'\test_copy_smb-dst.txt',
            dst=LOCAL_TEST_RESULTS.joinpath('test_copy_smb-dst2.txt'),
        )

        # Within SMB
        self._do(
            func=filemgr.copy,
            src=SMB_TEST_RESULTS + r'\test_copy_smb-dst.txt',
            dst=SMB_TEST_RESULTS + r'\test_copy_smb-dst2.txt',
        )

    
    def test_copy2_local(self):
        if not SMB_TEST_RESULTS:
            return self.skipTest('smb not available')

        self._do(
            func=filemgr.copy2,
            src=LOCAL_TEST_RESULTS.joinpath('test_copy2_local-src.txt'),
            create_src=True,
            dst=LOCAL_TEST_RESULTS.joinpath('test_copy2_local-dst.txt'),
            expect_same_mtime=True,
        )


    def test_copy2_smb(self):        
        if not SMB_TEST_RESULTS:
            return self.skipTest('smb not available')

        # To SMB
        self._do(
            func=filemgr.copy2,
            src=LOCAL_TEST_RESULTS.joinpath('test_copy2_smb-src.txt'),
            create_src=True,
            dst=SMB_TEST_RESULTS + r'\test_copy2_smb-dst.txt',
            expect_same_mtime=True,
        )

        # From SMB
        self._do(
            func=filemgr.copy2,
            src=SMB_TEST_RESULTS + r'\test_copy2_smb-dst.txt',
            dst=LOCAL_TEST_RESULTS.joinpath('test_copy2_smb-dst2.txt'),
            expect_same_mtime=True,
        )

        # Within SMB
        self._do(
            func=filemgr.copy2,
            src=SMB_TEST_RESULTS + r'\test_copy2_smb-dst.txt',
            dst=SMB_TEST_RESULTS + r'\test_copy2_smb-dst2.txt',
            expect_same_mtime=True,
        )

    
    def test_copyfile_local(self):        
        if not SMB_TEST_RESULTS:
            return self.skipTest('smb not available')

        self._do(
            func=filemgr.copyfile,
            src=LOCAL_TEST_RESULTS.joinpath('test_copyfile_local-src.txt'),
            create_src=True,
            dst=LOCAL_TEST_RESULTS.joinpath('test_copyfile_local-dst.txt'),
        )


    def test_copyfile_smb(self):        
        if not SMB_TEST_RESULTS:
            return self.skipTest('smb not available')

        # To SMB
        self._do(
            func=filemgr.copyfile,
            src=LOCAL_TEST_RESULTS.joinpath('test_copyfile_smb-src.txt'),
            create_src=True,
            dst=SMB_TEST_RESULTS + r'\test_copyfile_smb-dst.txt',
        )

        # From SMB
        self._do(
            func=filemgr.copyfile,
            src=SMB_TEST_RESULTS + r'\test_copyfile_smb-dst.txt',
            dst=LOCAL_TEST_RESULTS.joinpath('test_copyfile_smb-dst2.txt'),
        )

        # Within SMB
        self._do(
            func=filemgr.copyfile,
            src=SMB_TEST_RESULTS + r'\test_copyfile_smb-dst.txt',
            dst=SMB_TEST_RESULTS + r'\test_copyfile_smb-dst2.txt',
        )


    def _do(self, src, dst, func, create_src=False, expect_same_mtime=False):
        text = 'Hello,\nWorld.\n'

        if create_src:
            if filemgr.exists(src):
                filemgr.remove(src)
            filemgr.write_text(src, text, encoding='utf-8')

        stat_src = filemgr.stat(src)

        if expect_same_mtime:
            sleep(0.100)
        
        if filemgr.exists(dst):
            filemgr.remove(dst)
            
        func(src, dst)

        stat_dst = filemgr.stat(dst)


        self.assertTrue(filemgr.exists(dst))
        self.assertEqual(text, filemgr.read_text(dst))

        if expect_same_mtime:
            self.assertEqual(stat_dst.st_mtime, stat_src.st_mtime)
