from __future__ import annotations
from unittest import TestCase
from io import StringIO
from enum import Enum
from flexout import flexout, TabulateOut


class Case(TestCase):
    def setUp(self):
        if not TabulateOut.is_available():
            return self.skipTest('TabulateOut not available')

    
    def test_noheaders(self):
        out = StringIO()

        class AnEnum(Enum):
            VAL = 1

        with flexout(out, format=TabulateOut) as o:
            o.print_title('test', out=out)
            o.headers = ['col1', 'col2', 'col3']
            o.append(['Text', 3.14, None])
            o.append(['', 20, AnEnum.VAL])

        self.assertEqual(out.getvalue(), """
########## Test ##########

col1      col2  col3
------  ------  ------
Text      3.14
         20     VAL
""")
