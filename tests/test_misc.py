from __future__ import annotations
from unittest import TestCase
from io import StringIO
from datetime import date, datetime
from pathlib import Path
from flexout import flexout, TabulateOut
from tests.settings import LOCAL_TEST_RESULTS


EXPECTED_CSV = """Id;Name;Created on;Last at
1;"One, Two; Three: ""GO\"\"\";2022-03-26;
2;Me too;2022-03-26;1998-07-12 23:46:00+02:00
3;"<a href=""http://www.symbolics.com"">Lien</a>";2022-03-26;1998-07-12 00:46:00
"""

EXPECTED_TABULATE = """  Id  Name                                         Created on    Last at
----  -------------------------------------------  ------------  --------------------------------
   1  One, Two; Three: "GO"                        2022-03-26
   2  Me too                                       2022-03-26    1998-07-12 23:46:00.123000+02:00
   3  <a href="http://www.symbolics.com">Lien</a>  2022-03-26    1998-07-12 00:46:00.123000
"""


def _export(outfile, **options):
    with flexout(outfile, headers=["Id", "Name", "Created on", "Last at"], **options) as o:
        o.append(1, "One, Two; Three: \"GO\"", date(2022, 3, 26), None)
        o.append(2, "Me too", date(2022, 3, 26), datetime.fromisoformat('1998-07-12T23:46:00.123+02:00'))
        o.append(3, "<a href=\"http://www.symbolics.com\">Lien</a>", date(2022, 3, 26), datetime.fromisoformat('1998-07-12T00:46:00.123'))


def _export_using_dict(outfile, **options):
    with flexout(outfile, **options) as o:
        o.append({"Id": 1, "Name": "One, Two; Three: \"GO\"", "Created on": date(2022, 3, 26)})
        o.append({"Id": 2, "Created on": date(2022, 3, 26), "Last at": datetime.fromisoformat('1998-07-12T23:46:00.123+02:00'), "Name": "Me too"})
        o.append({"Id": 3, "Name": "<a href=\"http://www.symbolics.com\">Lien</a>", "Created on": date(2022, 3, 26), "Last at": datetime.fromisoformat('1998-07-12T00:46:00.123')})


class Case(TestCase):
    def test_tabulate(self):
        if not TabulateOut.is_available():
            return self.skipTest('TabulateOut not available')

        outfile = StringIO()
        _export(outfile, format='tabulate')
        
        outfile.seek(0)
        content = outfile.read()
        self.assertEqual(content, EXPECTED_TABULATE)


    def test_tabulate_using_dict(self):
        if not TabulateOut.is_available():
            return self.skipTest('TabulateOut not available')

        outfile = StringIO()
        _export_using_dict(outfile, format='tabulate')
        
        outfile.seek(0)
        content = outfile.read()
        self.assertEqual(content, EXPECTED_TABULATE)


    def test_csv(self):
        target = LOCAL_TEST_RESULTS.joinpath("output.csv")
        _export(target, dialect="excel-fr")

        actual_text = Path(target).read_text(encoding='utf-8-sig')
        self.assertEqual(actual_text, EXPECTED_CSV)


    def test_csv_with_dict(self):
        target = LOCAL_TEST_RESULTS.joinpath("output-with-dict.csv")
        _export_using_dict(target, dialect="excel-fr")

        actual_text = Path(target).read_text(encoding='utf-8-sig')
        self.assertEqual(actual_text, EXPECTED_CSV)
