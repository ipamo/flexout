from __future__ import annotations
import os
from pathlib import Path
from flexout import filemgr
from flexout.filemgr import can_use_network_paths
from dotenv import load_dotenv

load_dotenv()

SAMPLES = Path(__file__).parent.joinpath('samples')

LOCAL_TEST_RESULTS = Path(__file__).parent.joinpath("results")
LOCAL_TEST_RESULTS.mkdir(parents=True, exist_ok=True)

SMB_USER = os.environ.get('SMB_USER', None)
SMB_PASSWORD = os.environ.get('SMB_PASSWORD', None)
if SMB_USER or SMB_PASSWORD:
    filemgr.configure_smb_credentials(user=SMB_USER, password=SMB_PASSWORD)

SMB_TEST_RESULTS = os.environ.get('SMB_TEST_RESULTS', None)
if SMB_TEST_RESULTS:
    if not can_use_network_paths():
        SMB_TEST_RESULTS = None
    else:
        filemgr.makedirs(SMB_TEST_RESULTS, exist_ok=True)
