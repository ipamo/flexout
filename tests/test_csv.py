from __future__ import annotations
import logging
from unittest import TestCase
from pathlib import Path
from io import StringIO
from datetime import datetime
from flexout import flexout, CsvOut, filemgr
from tests.settings import LOCAL_TEST_RESULTS, SMB_TEST_RESULTS


class Case(TestCase):
    def test_noheaders(self):
        out = StringIO()

        with flexout(out, format=CsvOut, dialect='excel-fr') as o:
            o.append(['Text', '"Quote"', 'New\r\nLine', 1, 3.14, '', None])
        
        self.assertEqual(out.getvalue(), 'Text;"""Quote""";"New\r\nLine";1;3,14;;\r\n')


    def test_headers(self):
        out = StringIO()

        with flexout(out, format=CsvOut, dialect='excel') as o:
            o.headers = ['col1', 'col2', 'missing']
            o.append(['val1', 'val2']) # None will be added
            o.append(['val1', 'val2', 'missing', 'extra'])  # Extra will be added anyway
            o.append(['val1', 'val2']) # None will be added
        
        self.assertEqual(out.getvalue(), 'col1,col2,missing\r\nval1,val2,\r\nval1,val2,missing,extra\r\nval1,val2,\r\n')


    def test_dicts(self):
        out = StringIO()
        expected = 'col1,col2,col3\r\n'

        with self.assertLogs('flexout', logging.WARNING):
            with flexout(out, format=CsvOut, dialect='excel') as o:
                o.append({'col1': 11, 'col2': 12})
                expected += '11,12,\r\n'

                o.append({'col3': 23, 'col2': 22})
                expected += ',22,23\r\n'

                o.append({'col1': 31, 'col3': 33})
                expected += '31,,33\r\n'

                o.append([41, 42])
                expected += '41,42,\r\n'

                o.append({'col1': 51, 'col4': 54})
                expected += '51,,\r\n'
            
            self.assertEqual(out.getvalue(), expected)


    def test_append_local(self):
        self._append(LOCAL_TEST_RESULTS.joinpath('append.csv'))


    def test_append_smb(self):
        if not SMB_TEST_RESULTS:
            return self.skipTest('smb not available')
        self._append(SMB_TEST_RESULTS + r'\append.csv')


    def test_timezone(self):
        out = StringIO()
        with flexout(out, format='csv', dialect='excel-fr') as o:
            o.headers = ['Col']
            o.append(datetime.fromisoformat('1998-07-12T21:46:00.123+02:00'))

        self.assertEqual(out.getvalue(), "Col\r\n1998-07-12 21:46:00+02:00\r\n")


        out = StringIO()
        with flexout(out, format='csv', dialect='excel-fr', timezone='local') as o:
            o.headers = ['Col']
            o.append(datetime.fromisoformat('1998-07-12T21:46:00.123+02:00'))

        self.assertEqual(out.getvalue(), "Col\r\n1998-07-12 21:46:00\r\n")


        out = StringIO()
        with flexout(out, format='csv', dialect='excel-fr', timezone='utc') as o:
            o.headers = ['Col']
            o.append(datetime.fromisoformat('1998-07-12T21:46:00.123+02:00'))

        self.assertEqual(out.getvalue(), "Col\r\n1998-07-12 19:46:00\r\n")



    def _append(self, out: Path|str):
        expected = 'previous;col2;col1\r\n;02;01\r\n'

        if not isinstance(out, str):
            out = str(out)
        
        with filemgr.open_file(out, 'w', newline='', mkdir=True) as f:
            f.write(expected)
                
        with self.assertLogs('flexout.csv', logging.WARNING):
            with flexout(out, append=True, format=CsvOut, dialect='excel-fr') as o:
                o.headers = ['col1', 'col2', 'col3']

                o.append(11, 12, 13)
                expected += ';12;11;13\r\n'

            with filemgr.open_file(out, 'r', newline='') as f:
                actual = f.read()
            self.assertEqual(actual, expected)
